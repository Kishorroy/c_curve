#include<windows.h>
#include <GL/glut.h>
#include <math.h>
#include<bits/stdc++.h>
using namespace std;
#define pi      2*acos(0)

float toRa=0.0174532925;
int wn;

void cCurve(float x,float y,float len,float ang,int n) {
    float rAng=toRa*ang;
    if(n==0)
    {
        float x1=x+len*cos(rAng);
        float y1=y+len*sin(rAng);
        glVertex2f(x, y);
		glVertex2f(x1, y1);
		return;

    }
    len/=sqrt(2.0);
        rAng=toRa*(ang+45);
		cCurve(x,y,len,ang+45,n-1);
		x+=len*cos(rAng);
		y+=len*sin(rAng);
        cCurve(x,y,len,ang-45,n-1);
}

void mydisplay(){
    glClearColor(1,1,1,1);
	 glClear( GL_COLOR_BUFFER_BIT );
	 glBegin(GL_LINES);
	 glColor3f(1.0, 0.0, 0.0); // make it red

    cCurve(0.0,0.05,.2,90,wn);
	 glEnd();
	 glFlush();
}

int main(int argc, char** argv)
{
    cin>>wn;
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
	glutInitWindowSize(700,700);
	glutInitWindowPosition(0,0);
	glutCreateWindow("L�vy C Curve");
	glutDisplayFunc(mydisplay);
	glutMainLoop();
}
